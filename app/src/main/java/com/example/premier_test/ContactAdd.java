package com.example.premier_test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ContactAdd extends AppCompatActivity {

    private static ArrayList<Contact> contacts = new ArrayList<Contact>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);

        Intent ii = getIntent();
        Bundle b = ii.getExtras();

        Contact contact = new Contact(b.get("contact-nom").toString(), b.get("contact-prenom").toString(), b.get("contact-tel").toString());
        contacts.add(contact);

        ArrayAdapter<Contact> adaptater = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contacts);

        ListView listView = (ListView)findViewById(R.id.lv);
        listView.setAdapter(adaptater);

        Button boutonRetour = (Button)findViewById(R.id.boutonRetour);
        boutonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousWindow();
            }
        });
    }

    public void previousWindow () {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}

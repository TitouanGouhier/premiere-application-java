package com.example.premier_test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button boutonOk = (Button)findViewById(R.id.boutonOk);

        EditText nom = (EditText)findViewById(R.id.nom);
        String name = nom.getText().toString();

        EditText prenom = (EditText)findViewById(R.id.prenom);
        String surname = prenom.getText().toString();

        final EditText tel = (EditText)findViewById(R.id.tel);
        String phone = tel.getText().toString();


        Button bt = (Button)findViewById(R.id.bt);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousWindow();
            }
        });
        boutonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nom = (EditText)findViewById(R.id.nom);
                String name = nom.getText().toString();

                EditText prenom = (EditText)findViewById(R.id.prenom);
                String surname = prenom.getText().toString();

                final EditText tel = (EditText)findViewById(R.id.tel);
                String phone = tel.getText().toString();

                String jemalalatete = name + surname + phone;

                writeToFile(jemalalatete, MainActivity.this);
                readFromFile(MainActivity.this);
                if(!name.equals("") && !surname.equals("") && !phone.equals("")) {
                    Intent i = new Intent(MainActivity.this, ContactAdd.class);
                    i.putExtra("contact-nom", name);
                    i.putExtra("contact-prenom", surname);
                    i.putExtra("contact-tel", phone);
                    startActivity(i);

                }else{
                    Toast.makeText(MainActivity.this, "Remplis les champs wola ", Toast.LENGTH_SHORT).show();
                }
            }
        });
}
    public void previousWindow () {
        Intent i = new Intent(this, Main3Activity.class);
        startActivity(i);
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contactList.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("contactList.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }
}

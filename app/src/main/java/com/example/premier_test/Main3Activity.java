package com.example.premier_test;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {
        EditText editName,editSurname, editphone;
        Button btnAddData;
        Button btnViewAll;
        DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        myDb = new DatabaseHelper(this);

        editName = (EditText) findViewById(R.id.editText_name);
        editSurname = (EditText) findViewById(R.id.editText_surname);
        editphone = (EditText) findViewById(R.id.editText_phone);
        btnAddData = (Button) findViewById(R.id.button_add);
        btnViewAll = (Button) findViewById(R.id.button_viewAll);
        AddData();
    }
    public void AddData(){
        btnAddData.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Boolean isinserted = myDb.insertData(editName.getText().toString(),editSurname.getText().toString(),editphone.getText().toString());
                        if(isinserted = true)
                            Toast.makeText(Main3Activity.this, "Data inserted", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Main3Activity.this, "Data not inserted", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
    public void ViewAll(){
        btnViewAll.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        myDb.getAllData();
                        Cursor res = myDb.getAllData();
                        if (res.getCount() == 0) {
                            showMessage("Error","Nothing found");
                            return;
                        }
                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("Id :" + res.getString(0)+"\n");
                            buffer.append("Name :" + res.getString(1)+"\n");
                            buffer.append("Surname :" + res.getString(2)+"\n");
                            buffer.append("Phone :" + res.getString(3)+"\n\n");
                        }
                        showMessage("Data",buffer.toString());
                    }
                }
        );
    }
    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
    }
}

//package com.example.premier_test;
//
//import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//
//    class bddSqlite extends SQLiteOpenHelper {
//
//        public bddSqlite(Context context, String nom,
//                         SQLiteDatabase.CursorFactory cursorFactory , int version) {
//            super(context, nom, cursorFactory, version);
//        }
//
//        @Override
//        public void onCreate(SQLiteDatabase db) {
//            db.execSQL(REQUETE_CREATION_BD);
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//            db.execSQL("drop table" + TABLE_PLANETES + ";");
//            onCreate(db);
//        }
//
//        private static final int BASE_VERSION = 1;
//        private static final String BASE_NOM = "planetes.db";
//        private static final String TABLE_PLANETES = "table_planetes";
//        public static final String COLONNE_ID = "id";
//        public static final int COLONNE_ID_ID = 0;
//        public static final String COLONNE_NOM = "nom";
//        public static final int COLONNE_NOM_ID = 1;
//        public static final String COLONNE_RAYON = "rayon";
//        public static final int COLONNE_RAYON_ID = 2;
//
//        private static final String REQUETE_CREATION_BD = "create table "
//                + TABLE_PLANETES +" (" + COLONNE_ID
//                + " integer primary key autoincrement, " + COLONNE_NOM
//                + " text not null," + COLONNE_RAYON + " text not null);";
//
//        private SQLiteDatabase maBaseDonnees;
//
//        private bddSqlite baseHelper;
//
//        public PlanetesDBAdaptateur(Context ctx){
//            baseHelper = new MaBaseOpenHelper(ctx,BASE_NOM,null,BASE_VERSION);
//        }
//        public SQLiteDatabase open() {
//            maBaseDonnees = baseHelper.getWritableDatabase();
//            return maBaseDonnees;
//        }
//}